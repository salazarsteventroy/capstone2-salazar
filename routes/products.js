const express = require ("express");
const router = express.Router();
const productsController = require ("../controllers/products");
const auth = require ("../auth");

/*router.post ("/addProduct", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	productsController.addProduct(req.body, {productId: productData.id, isAdmin:userData.isAdmin}).then(resultFromController => res.send(resultFromController))
})*/

router.post ("/addProduct", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	productsController.addProduct(req.body, {userId: userData.id, isAdmin: userData.isAdmin}).then(resultFromController => res.send (resultFromController))
})

router.get("/Products", (req, res) => {
	productsController.getAllActive().then(
		resultFromController => res.send (
			resultFromController));
})
// 
router.get("/:productId", (req,res) => {
	productsController.getProduct(req.params).then(
		resultFromController => res.send (
			resultFromController))
})
// 
router.put("/:productId", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization)
	productsController.updateProduct(req.params, req.body, {userId: userData.id, isAdmin: userData.isAdmin}).then(resultFromController => res.send (resultFromController))
})

router.put("/:productId/archive", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization)

	productsController.archiveProduct(req.params, req.body, {userId: userData.id, isAdmin: userData.isAdmin}).then(resultFromController => res.send (resultFromController))
})


module.exports = router;
