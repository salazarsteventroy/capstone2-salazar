const express = require('express');
const router = express.Router();
const auth = require("../auth");

const userController = require('../controllers/user')


router.post('/register', (req, res) => {
	userController.registerUser(req.body).then(result => res.send(result));
})

router.post('/login', (req, res) => {
	userController.loginUser(req.body).then(result => res.send(result))
})

// aaaaaaaaaaaaaaaa

router.post ('/checkout', (req,res) => {
	let data = {
		userId : req.body.userId,
		ProductId : req.body.ProductId
	}

	userController.order(data).then(resultFromController => res.send(resultFromController))
})

router.get ("/allProduct", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization)

	userController.getAllOrder().then(resultFromController => res.send (resultFromController));
})

router.get ("getOrder", auth.verify, (req,res) => {
	const userData = auth.decode (req.headers.authorization)
	userController.getOrder(req.body, {userId: userData.id, isAdmin: userData.isAdmin}).then (resultFromController => res.send (resultFromController));
})


/*router.put("/:userId/makeadmin", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization)
	userController.makeAdmin(req.params, {userId : userData.id,isAdmin: userData.isAdmin}).then(resultFromController => res.send(resultFromController))
})*/


router.put("/:userId/makeadmin", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization)

	userController.makeAdmin(req.params, req.body, {userId: userData.id, isAdmin: userData.isAdmin}).then(resultFromController => res.send (resultFromController))
})

module.exports = router;



