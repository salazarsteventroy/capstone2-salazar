const Products = require ("../models/products");

module.exports.addProduct = (reqBody, userData) => {
	return Products.findById(userData.userId).then(result => {
		if(userData.isAdmin){
			return ("you are not an admin")
		} else {
			let newProduct = new Products ({
				name: reqBody.name,
				description : reqBody.description,
				price: reqBody.price
			})

			return newProduct.save().then((Products,error) => {
				if (error) {
					return false
				}else {
					return ("Product Created")
				}
			})
		}
	})
}

module.exports.updateProduct = (reqParams, reqBody, userData) => {
	return Products.findById(userData.userId).then(result => {
		if (userData.isAdmin) {
			return "you are not an admin"
		}else {
			let updatedProduct = {
				name: reqBody.name,
				description : reqBody.description,
				price: reqBody.price
			}
			return Products.findByIdAndUpdate(
				reqParams.ProductId, updatedProduct).then((Products, error) => {
					if (error){
						return false;
					}else {
						return true;
					}
				})
		}
	})
}



module.exports.getAllActive = () => {
	return Products.find({isActive: true}).then(result => {
		return result;
	})
}

module.exports.getProduct = (reqParams) => {
	return Products.findById(reqParams.ProductId).then(result => {
		return result;
	})
}


module.exports.archiveProduct = (reqParams, reqBody, userData) => {
	return Products.findById(userData.userId).then(result => {
		if (userData.isAdmin){
			return ("you are not an admin")
		} else {
			let updatedActiveField = {
				isActive : false
			};
			return Products.findByIdAndUpdate(reqParams.ProductId,
				updatedActiveField).then((Products, error) => {
					if (error){
						return false;
					}else {
						return true;
					}
				})
		}
	})
}


// 
module.exports.makeadmin = (reqParams, reqBody, userData) => {
	return User.findById(userData.userId).then(result => {
		if (userData.isAdmin){
			return ("you are not an admin")
		} else {
			let userAdmin = {
				isAdmin : true
			};
			return User.findByIdAndUpdate(reqParams.UserId,
				userAdmin).then((users, error) => {
					if (error){
						return false;
					}else {
						return true;
					}
				})
		}
	})
}


