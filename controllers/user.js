const User = require ('../models/user');
const product = require ('../models/products');
const bcrypt = require ('bcrypt');
const auth = require ('../auth');

module.exports.registerUser = (reqBody) => {
	let newUser = new User ({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password,10)
	})

	return newUser.save().then((user,error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}




module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if (result == null){
			return false
		}else {
			const passwordCorret = bcrypt.compareSync(reqBody.password,result.password)
			if(passwordCorret){
				return {accessToken: auth.createAccessToken(result)}
			}else {
				return false
			}
		}
	})
}

// checkout

module.exports.order = async (data) => {
	let findUser = await User.findById(data.userId).then(user => {
		user.checkout.push({ProductId : data.ProductId})

		return user.save().then((user,error) => {
			if (error){
				return false;

			}else {
				return true;
			}
		})
	})

		let orderMade = await Products.findById(data.ProductId).then(Products => {
			products.productInfo.push({userId : userData});

		return products.save().then((Products, error) => {
			if(error){
				return false;
			}else {
				return true;
			}
		})
		})
	}



module.exports.getAllOrder = (userData) => {
	return User.findById(userData.userId).then(result => {
		if (userData.isAdmin) {
			return "you are not an admin"
		}else {
			return User.find({}).then (result => {
				return result;
			})
		}
	})
}

// retrieve authenticated user's order

module.exports.getOrder = (reqParams, userData) => {
	return User.findById(userData.userId).then(result => {
		return result;
	})
}

// 
module.exports.makeAdmin = (reqParams, reqBody, userData) => {
	return User.findById(userData.userId).then(result => {
		if (userData.isAdmin){
			return ("you are not an admin")
		} else {
			let userAdmin = {
				isAdmin : true
			};
			return User.findByIdAndUpdate(reqParams.UserId,
				userAdmin).then((users, error) => {
					if (error){
						return false;
					}else {
						return true;
					}
				})
		}
	})
}

