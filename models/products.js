const mongoose = require ('mongoose');

const productsSchema = new mongoose.Schema({

	name: {
		type: String,
		required: [true, "Product is required"]
	},
	description: {
		type: String,
		required: [true, "Description is required"]
	},
	price: {
		type: Number,
		required: [true, "Price is required"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn:{
		type: Date,
		default: new Date()
	},
	productInfo: [
	{
		purchasedOn: {
			type: Date,
			default: new Date()
		},
		productId: {
			type: String,
			required: true
		},
		totalAmount: {
			type: Number,
			default: new Number()
		}

	}
	]
})

module.exports = mongoose.model('products', productsSchema)