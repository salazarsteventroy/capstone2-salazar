const express = require ('express');
const mongoose = require ('mongoose');
const cors = require ('cors');

const userRoutes = require ('./routes/user')
const productsRoutes = require ('./routes/products')

const app = express();
const port = 3001;

app.use(cors());

app.use (express.json());
app.use(express.urlencoded({extended: true}));

app.use('/api/user', userRoutes)
app.use('/api/products', productsRoutes)

mongoose.connect("mongodb+srv://steventroy:steventroy@wdc028-course-booking.alyii.mongodb.net/batch-144-database?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"))
db.once("open", ()=> console.log ("we're connected to the cloud database"))


app.listen(port, () => console.log(`now listening to port ${port}`))


